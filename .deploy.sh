#!/bin/bash
set -x
WEB_BASE="/var/www/"

if [[ $CI_PIPELINE_SOURCE == "merge_request_event" ]]
then
    exit
fi

if [[ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]]
then
    TARGET_DIR="${WEB_BASE}/www.spline.de/htdocs/"
    URL="https://spline.de/"
    OPTS=""
else
    TARGET_DIR="${WEB_BASE}/branches.spline.de/htdocs/${CI_COMMIT_BRANCH}"
    URL="https://www.spline.de/_branch/${CI_COMMIT_BRANCH}/"
    OPTS="-D"
fi

mkdir -p "${TARGET_DIR}"
./hugo -F $OPTS -b "${URL}"
rsync -ahvP public/ "${TARGET_DIR}" --delete

