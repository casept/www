---
title: "Gitlab Update"
tags: ['gitlab', 'spline']
date: 2013-05-07T02:00:00+00:00
draft: false

---
Am Dienstag (07.05.2013) zwischen 16:00 und 18:00 wird der Dienst Gitlab auf
die aktuelle Version 5.1 aktualisiert. Es kann hierdurch zu Ausfällen kommen.
Wir bitten dies zu Entschuldigen und hoffen möglichst schnell wieder unseren
Git-Servier anbieten zu können.

Falls ihr in den kommenden Tagen Unregelmäßigkeiten oder andere Dinge bemerket,
meldet dies bitte per Mail an **spline@spline.de.**
