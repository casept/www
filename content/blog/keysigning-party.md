---
title: "Keysigning-Party"
tags: ['keysigning']
date: 2006-01-10T02:00:00+00:00
draft: false

---

## Zeit & Ort

Dienstag, 31. Januar 2006, von 17:30 bis 19:00
Informatik-Gebäude, Takustraße 9, Raum 053

## Keysigning?
Eine Keysigning-Party ist ein lockeres Treffen von Nutzern und Nutzerinnen des PGP-Verschlüsselungsverfahrens. Die Teilnehmer der Party überprüfen ihre Identität und signieren gegenseitig ihre öffentlichen PGP-Schlüssel. Als Resultat einer solchen Party entsteht ein Web-of-Trust, also ein Netz von Nutzern, die gegenseitig die Beziehung zwischen PGP-Schlüssel und Identität bestätigen.

* Lokales Web-of-Trust einer Keysigning-Party.

Keysigning findet überall auf der Welt statt und so ist es kein Wunder, dass viele kleine lokale Netze zu einem weltweiten Web-of-Trust verbunden sind . Mit der Größe und Vermaschung dieses Netzes steigt die Aussagekraft von PGP-Signaturen, z.B. durch das Verfolgen von Vertrauenspfaden

* Vertrauenspfad durch das Web-of-Trust von Gero zu Konrad.

Wer also wünscht, dass andere seinen signierten Emails vertrauen, der sollte an einer Keysigning-Party teilnehmen. Hier kann die Beziehung zwischen PGP-Schlüssel und Identität von vielen unabhängigen Personen dezentral bestätigt werden und das kostenlos.
Weitere und ausführlichere Informationen zu Keysigning-Partys finden sich im deutschen und englischen HowTo von GnuPG, einer freien und sehr umfangreichen Implementierung von PGP.

## Teilnehmen
Zu unserer Keysigning-Party sind alle Interessierten herzlich eingeladen. Wer aktiv teilnehmen möchte, sollte allerdings die folgenden Punkte beachten:
* Falls noch nicht Vorhanden, muss ein PGP-Schlüsselpaar (privater und öffentlicher Schlüssel) generiert werden. Eine ausführliche deutsche und englische Anleitung findet sich im GnuPG-HowTo. Für Fragen steht Spline zu Verfügung.
* Der eigene, öffentliche PGP-Schlüssel muss vor der Party an eine der beiden folgenden Emailadressen gesendet werden, da nur so zur Party alle Schlüssel-Fingerprints für alle Teilnehmer ausgedruckt werden können.
* * kr (at) spline.inf.fu-berlin.de
* * bogus (at) spline.inf.fu-berlin.de

Zur Party sind die folgenden Dinge mitzubringen.
* Personalausweiß oder Pass
* Ausdruck der eigenen Schlüssel-ID und des Schlüssel-Fingerprints
* Etwas zum Schreiben, z.B. Stift und Papier

Computer sollten falls möglich nicht eingesetzt oder mitgebracht werden, da es auf einer Keysigning-Party viele Möglichkeiten gibt, Böses zu tun, wie z.B. das Erspähen von Passwörtern oder das Einschleusen von "Hintertüren".
Desweiteren dürfen neben diesen lästigen Punkten natürlich nicht die gute Laune und der Spaß zu Hause vergessen werden.

## Ablauf
Die Keysigning-Party wird in drei Phasen durchgeführt. Nach einer kurzen und mathematikfreien Einführung zu den Grundlagen von Kryptographie und PGP, geht es mit der ersten Phase los.

* Die Teilnehmer erhalten die Schlüssel-ID und -Fingerprints aller anderen Teilnehmer als Ausdruck. Es wird nun jede Schlüssel-ID und der entsprechende Fingerprint vorgelesen und der Besitzer des Schlüssels prüft, ob es sich tatsächlich um seinen Schlüssel handelt. Ist dies der Fall, so markieren alle Teilnehmer die Schlüssel-ID und den Fingerprint als korrekt.
* In der zweiten Phase werden die Teilnehmer der Reihe nach aufgerufen und gebeten, den anderen ihren Personalausweiß zu zeigen, so dass alle die Identität bestätigen können. Stimmt die Identität überein, markieren die Teilnehmer die Schlüssel-ID und den entsprechenden Fingerprint ein zweitesmal.

Nachdem alles überprüft und markiert wurde ist der offizielle Teil beendet und die Party kann auf verschiedene Weise, z.B. in einer Gaststätte der Wahl, fortgesetzt werden. Die entscheidende dritte Phase, das eigentliche Keysigning, wird von jedem Teilnehmer zu Hause und alleine durchgeführt.

* Jeder Teilnehmer importiert und signiert mit seinem privaten PGP-Schlüssel alle Schlüssel, die auf der Party zweifach markiert wurden. Anschließend sendet er die signierten Schlüssel an einen öffentlichen Schlüsselserver oder per Email an den Besitzer zurück. Eine deutsche und englische Anleitung zum Signieren von PGP-Schlüsseln ist ebenfalls Teil des GnuPG-HowTos.

**UPDATE:** entstandene Web-of-Trust (3.3.2006)

![Web-of-Trust](/images/blog/web-of-trust-jan-2006.png)
