---
title: Gitlab
logo_url: /images/services/gitlab.png
description: Git-Hosting mit Ticket-System und Wiki
link: http://gitlab.spline.de
---