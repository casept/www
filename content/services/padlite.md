---
title: Pad-Lite
logo_url: /images/services/pad.png
description: kollaborativer Texteditor Lite (EtherPadLite)
link: http://padlite.spline.de
---