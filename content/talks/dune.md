---
title: DUNE
location: Hörsaal der Informatik
time: 18:00 Uhr
date: 2010-12-01T12:00:00+01:00
---
DUNE ist ein Satz von C++-Bibliotheken zur Lösung von partiellen
Differentialgleichungen. Es wird als Open-Source-Software von den Unis in
Heidelberg, Freiburg, Münster, und der FU Berlin entwickelt, und aktuell von
etwa 10 weiteren Gruppen in der numerischen Mathematik verwendet. Die Software
enthält viele für Informatiker spannende Probleme, z.B. sehr komplexe
Datenstrukturen, höchste Ansprüche an Effizienz, und den Einsatz auf
verschiedenen Rechnerplattformen bis hin zu Supercomputern. Der Vortrag soll
das zugrundeliegende Design erklären, und den Entwicklungsprozess in einem
akademischen Umfeld beschreiben.

Beachte: Obwohl es um Software für numerische Mathematik geht ist der Vortrag
selbst quasi komplett mathematikfrei!

## Vortragende
[Oliver Sander](http://page.mi.fu-berlin.de/sander/)

## Slides
[dune-project.pdf](/talks/dune-project.pdf)

## Links
* [DUNE](http://dune-project.org/)